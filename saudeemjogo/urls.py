"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from saudeemjogo import views

prefix = 'api/'

urlpatterns = [
    path(prefix+'admin/', admin.site.urls),
    path(prefix+'all_medicos/', views.get_all_medicos),
    path(prefix+'all_modulos/', views.get_all_modulos),
    path(prefix+'all_hospitais/', views.get_all_hospitais),
    path(prefix+'reembolsar_modulo/<int:hospital>/<int:modulo>', views.reembolsar_modulo),
    path(prefix+'comprar_modulo/<int:hospital>/<int:modulo>', views.comprar_modulo),
    path(prefix+'atribuir_medico_ao_modulo/<int:hospital>/<int:medico>/<int:modulo>', views.atribuir_medico_ao_modulo),
    path(prefix+'all_configs', views.get_all_configs),
    path(prefix+'get_config/<int:config>', views.get_configs),
    path(prefix+'submit_alternative_rank', views.add_aternative_rank),
    # path(prefix+'criar_hospital/<int:config>/<str:nome>', views.create_hospital),
    # path(prefix+'criar_lobby/<str:nome>', views.create_lobby),
    # path(prefix+'join_lobby/<str:lobby>/<str:hospital>', views.join_lobby),
    # path(prefix+'add_medico/<str:nome>/<int:perfil>/<int:salario>/<int:expertise>/<int:atendimento>/<int:pontualidade>', views.add_medico),
    # path(prefix+'add_modulo/<int:custo>/<int:custo_mes>/<int:qualidade>/<int:conforto>/<int:capacidade>/<int:preco>/<int:tipo>', views.add_modulo)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
