"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import CASCADE
from saudeemjogo.strings import *
import uuid
import re


# class ScoreModulo(models.Model):
# 	A = models.IntegerField(default=0)
# 	B = models.IntegerField(default=0)
# 	C = models.IntegerField(default=0)
# 	D = models.IntegerField(default=0)
# 	E = models.IntegerField(default=0)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"A": self.A,
# 				"B": self.B,
# 				"C": self.C,
# 				"D": self.D,
# 				"E": self.E
# 			}
#
#
# class ScoreDemanda(models.Model):
# 	card = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	emerge = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	psico = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	neuro = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	pedi = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"card": self.card.to_dict(),
# 				"emerg": self.emerge.to_dict(),
# 				"psico": self.psico.to_dict(),
# 				"neuro": self.neuro.to_dict(),
# 				"pedi": self.pedi.to_dict(),
# 			}
#
#
# class ScoreAtendidos(models.Model):
# 	demanda = models.ForeignKey(ScoreDemanda, on_delete=CASCADE)
# 	modulos = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"demanda": self.demanda.to_dict(),
# 				"modulos": self.modulos.to_dict()
# 			}
#
#
# class ScoreGanhos(models.Model):
# 	atendimento = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	venda_modulos = models.IntegerField(default=0)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"atendimento": self.atendimento.to_dict(),
# 				"venda_modulos": self.venda_modulos
# 			}
#
#
# class ScoreGastos(models.Model):
# 	custo_mensal = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
# 	custo_aquisicao = models.IntegerField(default=0)
# 	salario_medicos = models.IntegerField(default=0)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"custo_mensal": self.custo_mensal.to_dict(),
# 				"custo_aquisicao": self.custo_aquisicao,
# 				"salario_medicos": self.salario_medicos
# 			}
#
#
# class Score(models.Model):
# 	atendidos = models.ForeignKey(ScoreAtendidos, on_delete=CASCADE)
# 	dinheiro_acumulado = models.IntegerField(default=0)
# 	ganhos = models.ForeignKey(ScoreGanhos, on_delete=CASCADE)
# 	lucro = models.IntegerField(default=0)
# 	referente_mes = models.IntegerField(default=1)
# 	valor_medio_atendimento = models.ForeignKey(ScoreModulo, on_delete=CASCADE)
#
# 	def to_dict(self):
# 		return \
# 			{
# 				"atendidos": self.atendidos.to_dict(),
# 				"dinheiro_acumulado": self.dinheiro_acumulado,
# 				"ganhos": self.ganhos.to_dict(),
# 				"lucro": self.lucro,
# 				"referente_mes": self.referente_mes,
# 				"valor_medio_atendimento": self.valor_medio_atendimento.to_dict()
# 			}
#
#
# class Rank(models.Model):
# 	nome = models.CharField(max_length=30)
# 	scores = models.ManyToManyField(Score)
#
# 	def save(self, *args, **kwargs):
# 		self.nome = re.sub('[^A-Za-z0-9]+', '', self.nome)
# 		super().save(*args, **kwargs)
#
# 	def to_dict(self):
# 		scores = []
# 		for i in self.scores.all():
# 			scores.append(str(i))
# 		return \
# 			{
# 				"nome": self.nome,
# 				"scores": scores
# 			}


class DocumentRank(models.Model):
	rank = models.TextField()


class Emprestimos(models.Model):
	valor = models.IntegerField(blank=False)

	class Meta:
		verbose_name = 'Empréstimo'
		verbose_name_plural = 'Empréstimos'

	def __str__(self):
		return str(self.valor)

	def to_dict(self):
		return \
			{
				"valor": self.valor,
			}


class Configuracoes(models.Model):
	"""
		Vários tipos de configurações
	"""
	nome = models.CharField(verbose_name="Nome da configuração", max_length=100)
	tempo_rodada = models.IntegerField(verbose_name="Tempo de cada rodada (minutos)", default=15)
	tempo_minimo_demitir_medico = models.IntegerField(verbose_name="Tempo mínimo para demitir médico (meses)",
													  default=2)
	tempo_maximo_para_venda_modulo = models.IntegerField(blank=False,
														 verbose_name="Tempo máximo para vender módulo (meses)",
														 default=7)
	porcentagem_reembolso_modulo = models.IntegerField(blank=False, default=40)
	emprestimos_possiveis = models.ManyToManyField(Emprestimos)
	caixa_inicial = models.IntegerField(default=25000)
	mes_pagar_emprestimo = models.IntegerField(default=8)
	mes_final = models.IntegerField(default=12)
	juros_do_emprestimo = models.IntegerField(default=30)

	class Meta:
		verbose_name = 'Configuração'
		verbose_name_plural = 'Configurações'

	def __str__(self):
		return self.nome

	def save(self, *args, **kwargs):
		self.nome = self.nome.title()
		super().save(*args, **kwargs)

	def get_emprestimos(self):
		emprestimos = []
		for i in self.emprestimos_possiveis.all():
			emprestimos.append(str(i))
		return emprestimos

	def to_simple_dict(self):
		return \
			{
				"id": self.id,
				"nome": self.nome
			}

	def to_dict(self):
		return \
			{
				"id": self.id,
				"nome": self.nome,
				"tempo_rodada": self.tempo_rodada,
				"tempo_minimo_demitir_medico": self.tempo_minimo_demitir_medico,
				"tempo_maximo_para_venda_modulo": self.tempo_maximo_para_venda_modulo,
				"porcentagem_reembolso_modulo": self.porcentagem_reembolso_modulo,
				"emprestimos_possiveis": self.get_emprestimos(),
				"caixa_inicial": self.caixa_inicial,
				"mes_pagar_emprestimo": self.mes_pagar_emprestimo,
				"juros_do_emprestimo": self.juros_do_emprestimo,
				"mes_final": self.mes_final
			}


class TiposModulo(models.Model):
	"""
		Tipos de módulos
	"""
	nome = models.CharField(max_length=80)

	class Meta:
		verbose_name = 'Tipos de Módulo'
		verbose_name_plural = 'Tipos de Módulos'

	def __str__(self):
		return self.nome

	def save(self, *args, **kwargs):
		self.nome = self.nome.title()
		super().save(*args, **kwargs)

	def to_dict(self):
		return \
			{
				"id": self.id,
				"tipo": self.nome
			}


class Medico(models.Model):
	nome = models.CharField(null=False, blank=False, max_length=80)
	perfil = models.IntegerField(null=True, blank=False)
	salario = models.IntegerField(null=True, blank=False)
	expertise = models.CharField(max_length=1)
	atendimento = models.CharField(max_length=1)
	pontualidade = models.CharField(max_length=1)

	def save(self, *args, **kwargs):
		if not self.expertise.isalpha():
			return \
				{
					err: "Expertise deve ser uma letra entre A e Z"
				}
		if not self.atendimento.isalpha():
			return \
				{
					err: "Atendimento deve ser uma letra entre A e Z"
				}
		if not self.pontualidade.isalpha():
			return \
				{
					err: "Pontualidade deve ser uma letra entre A e Z"
				}
		self.expertise = self.expertise.upper()
		self.pontualidade = self.pontualidade.upper()
		self.atendimento = self.atendimento.upper()
		super().save(*args, **kwargs)

	def __str__(self):
		return "Médico perfil: " + str(self.perfil)

	def to_dict(self):
		return \
			{
				"nome": self.nome,
				"id": self.id,
				"salario": self.salario,
				"expertise": self.expertise,
				"atendimento": self.atendimento,
				"pontualidade": self.pontualidade
			}


class Modulo(models.Model):
	"""
		Módulos comprados pelos hospitais
	"""
	nome = models.ForeignKey(TiposModulo, on_delete=CASCADE)
	custo_aquisicao = models.IntegerField(null=True, blank=False)
	custo_por_mes = models.IntegerField(null=True, blank=False)
	tecnologia = models.CharField(max_length=1)
	conforto = models.CharField(max_length=1)
	capacidade = models.IntegerField(null=True, blank=False)
	preco_do_tratamento = models.IntegerField(null=True, blank=False)

	def save(self, *args, **kwargs):
		if not self.tecnologia.isalpha():
			return \
				{
					err: "Tecnologia deve ser uma letra entre A e Z"
				}
		if not self.conforto.isalpha():
			return \
				{
					"Conforto deve ser uma letra entre A e Z"
				}
		self.conforto = self.conforto.upper()
		self.tecnologia = self.conforto.upper()
		super().save(*args, **kwargs)

	class Meta:
		verbose_name = 'Módulo'
		verbose_name_plural = 'Módulos'

	def __str__(self):
		return str(self.nome) + " " + str(self.id)

	def to_dict(self):
		return \
			{
				"id": self.id,
				"nome": str(self.nome),
				"custo_aquisicao": self.custo_aquisicao,
				"custo_por_mes": self.custo_por_mes,
				"tecnologia": self.tecnologia,
				"conforto": self.conforto,
				"capacidade": self.capacidade,
				"preco_do_tratamento": self.preco_do_tratamento,
			}


class MedicoModulo(models.Model):
	medico = models.ForeignKey(Medico, on_delete=CASCADE)
	modulo = models.ForeignKey(Modulo, on_delete=CASCADE)

	class Meta:
		verbose_name = 'Médico no módulo'
		verbose_name_plural = 'Médicos nos módulos'

	def __str__(self):
		return str(self.medico) + " em " + str(self.modulo)

	def to_dict(self):
		return \
			{
				"medico": self.medico.id,
				"modulo": self.modulo.id
			}


class Hospital(models.Model):
	"""
		Hospital organizado pelos jogadores
		Os jogadores alterarão diretamente os hospitais
	"""
	nome = models.CharField(max_length=80)
	caixa = models.IntegerField(null=True, blank=False, default=25000)
	emprestimos = models.ManyToManyField(Emprestimos, blank=True)
	modulos = models.ManyToManyField(Modulo, blank=True)
	medicos = models.ManyToManyField(Medico, blank=True)
	medicos_modulos = models.ManyToManyField(MedicoModulo, blank=True)
	hash = models.CharField(max_length=50, unique=True)

	class Meta:
		verbose_name = 'Hospital'
		verbose_name_plural = 'Hospitais'

	def __str__(self):
		return self.nome

	def criar_hospital(config, nome):
		try:
			cfg = Configuracoes.objects.get(id=config)
			h = Hospital(caixa=cfg.caixa_inicial, nome=nome, hash=str(uuid.uuid4()))
			h.save()
			return \
				{
					"hash": str(h.hash)
				}
		except ObjectDoesNotExist:
			return \
				{
					err: null_config
				}

	def get_emprestimos(self):
		emprestimos = []
		for i in self.emprestimos.all():
			emprestimos.append(str(i))
		return emprestimos

	def atribuir_medico_ao_modulo(self, medico, modulo):
		try:
			me = self.medicos.get(id=medico)
		except ObjectDoesNotExist:
			return \
				{
					err: "Médico com id " + str(modulo) + " não existe"
				}
		try:
			mo = self.modulos.get(id=modulo)
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com id " + str(modulo) + " não existe"
				}
		mm = self.medicos_modulos.filter(medico=me)
		if mm is not None:
			mm.delete()
			return \
				{
					"hospital": self.id,
					success: "Médico " + str(medico) + " removido do módulo " + str(modulo)
				}
		mm = MedicoModulo(medico=me, modulo=mo)
		mm.save()
		self.medicos_modulos.add(mm)
		return \
			{
				"hospital": self.id,
				success: "Médico " + str(medico) + " atribuido ao módulo " + str(modulo)
			}

	def comprar_modulo(self, modulo):
		try:
			m = Modulo.objects.get(id=modulo)
			if m in self.modulos.all():
				return \
					{
						success: "Módulo " + str(
							m.nome) + " já foi comprado anteriormente, e não pode ser comprado novamente por" + self.nome
					}
			valor = m.custo_aquisicao
			if self.realizar_pagamento(valor):
				self.modulos.add(m)
				return \
					{
						success: "Módulo " + str(m.nome) + " comprado por " + self.nome
					}
			else:
				return \
					{
						err: "Caixa insuficiente"
					}
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com o id" + str(modulo) + " não existe"
				}
		except Exception as e:
			return \
				{
					err: str(e)
				}

	def vender_modulo(self, modulo):
		# 40% de reembolso, poderíamos fazer um reembolso variável pra dar um grau
		reembolso = 40
		try:
			m = Modulo.objects.get(id=modulo)
			if m in self.modulos.all():
				self.modulos.remove(m)
			else:
				return \
					{
						success: "Módulo " + str(m.nome) + " não existe neste hospital"
					}
			valor = m.custo_aquisicao * (reembolso / 100)
			self.adicionar_dinheiro_ao_caixa(valor)
			return \
				{
					success: "Módulo " + str(m.nome) + " removido e reembolsado 40%"
				}
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com o id " + str(modulo) + " não existe"
				}
		except Exception as e:
			return \
				{
					err: str(e)
				}

	def realizar_pagamento(self, valor):
		if self.caixa - valor >= 0:
			self.caixa = self.caixa - valor
			self.save()
			return True
		return False

	def adicionar_dinheiro_ao_caixa(self, valor):
		self.caixa = self.caixa + valor
		self.save()

	def to_dict(self):
		modulos = {}
		medicos = {}
		medicos_modulos = {}
		i = 0
		for m in self.medicos.all():
			medicos[i] = m.to_dict()
			i += 1
		i = 0
		for m in self.modulos.all():
			modulos[i] = m.to_dict()
			i += 1
		i = 0
		for m in self.medicos_modulos.all():
			medicos_modulos[i] = m.to_dict()
			i += 1
		return \
			{
				"id": self.id,
				"hash": self.hash,
				"nome": self.nome,
				"emprestimos": self.get_emprestimos(),
				"modulos": modulos,
				"medicos": medicos,
				"medicos_modulos": medicos_modulos,
			}


class Lobby(models.Model):
	hash = models.CharField(max_length=50, primary_key=True)
	nome = models.CharField(max_length=200)
	hospitais = models.ManyToManyField(Hospital, blank=True)

	def criar_lobby(nome):
		h = str(uuid.uuid4())
		l = Lobby(nome=nome, hash=h)
		l.save()
		return \
			{
				"lobby": h
			}

	def join(self, hospital):
		try:
			h = Hospital.objects.get(hash=hospital)
			if h in self.hospitais.all():
				self.hospitais.remove(h)
				return {success: h.nome + " removido do lobby"}
			self.hospitais.add(h)
			return self.to_dict()
		except ObjectDoesNotExist:
			return \
				{
					err: not_exist_hosp
				}

	def start(self):
		pass

	def next(self, fase):
		pass

	def destroy(self):
		self.delete()

	def to_dict(self):
		hsp = {}
		hospitais = self.hospitais.all()
		i = 0
		for h in hospitais:
			hsp[i] = h.to_dict()

		return \
			{
				"hash": str(self.hash),
				"nome": str(self.nome),
				"hospitais": hsp
			}
