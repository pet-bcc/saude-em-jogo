"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

"""
	Arquivo usado para manter as principais strings usadas
"""

# Gerais
err = 'error'
success = 'success'

# Médicos
null_med = 'Nenhum médico cadastrado'

# Módulos
null_mod = 'Nenhum módulo cadastrado'
null_tip_mod = 'Nenhum tipo de módulo cadastrado'

# Hospitais
null_hosp = 'Nenhum hospital cadastrado'
not_exist_hosp = 'Hospital procurado não existe'

# Config
null_config = 'Nenhuma configuração com esse ID achada'

# Lobby
null_lobby = "Lobby não existe"
