"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""
import requests

import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from saudeemjogo import settings
from saudeemjogo.models import *
from saudeemjogo.strings import *


@csrf_exempt
def add_aternative_rank(request):
	if request.method == 'POST':
		try:
			data = json.loads(request.body.decode('utf-8'))
		except Exception as e:
			return JsonResponse({"content": e})
		try:
			recaptcha_response = data['g-recaptcha-response']
			cap = {
				'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
				'response': recaptcha_response
			}
			r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=cap)
			result = r.json()
			if result['success']:
				try:
					new_rank = DocumentRank(str(data))
					new_rank.save()
				except Exception as e:
					return JsonResponse(
						{"ewe": e}
					)
				return JsonResponse({"success": "deu bom"})
			else:
				return JsonResponse({"error": "invalid captcha"})
		except Exception as e:
			return JsonResponse({"error": str(e)})
	else:
		return JsonResponse({err: "Invalid method"})


def get_all_medicos(request):
	medicos = Medico.objects.all()
	all_medicos = []
	for m in medicos:
		all_medicos.append(m.to_dict())
	if not all_medicos:
		return JsonResponse({err: null_med})
	return JsonResponse({"medicos": all_medicos})


def get_all_modulos(request):
	all_modulos = []
	modulos = Modulo.objects.all()
	for m in modulos:
		all_modulos.append(m.to_dict())
	if not all_modulos:
		return JsonResponse({err: null_mod})
	return JsonResponse({"modulos": all_modulos})


def get_all_hospitais(request):
	all_hospitais = {}
	hospitais = Hospital.objects.all()
	i = 0
	for h in hospitais:
		all_hospitais[i] = h.to_dict()
		i += 1
	if not all_hospitais:
		return JsonResponse({err: null_hosp})
	return JsonResponse(all_hospitais)


def get_all_tipos_de_modulos(request):
	all_tipos = {}
	tipos = TiposModulo.objects.all()
	i = 0
	for t in tipos:
		all_tipos[i] = t.to_dict()
		i += 1
	if not all_tipos:
		return JsonResponse({err: null_tip_mod})
	return JsonResponse(all_tipos)


def transform_num_to_char(num):
	if num == 3:
		return 'A'
	elif num == 2:
		return 'B'
	elif num == 1:
		return 'C'
	else:
		return 'A'


def add_modulo(request, custo, custo_mes, qualidade, conforto, capacidade, preco, tipo):
	qualidade = transform_num_to_char(qualidade)
	conforto = transform_num_to_char(conforto)
	tipo_mod = TiposModulo.objects.get(id=tipo)
	m = Modulo(nome=tipo_mod,
			   custo_aquisicao=custo,
			   custo_por_mes=custo_mes,
			   tecnologia=qualidade,
			   conforto=conforto,
			   capacidade=capacidade,
			   preco_do_tratamento=preco)
	m.save()
	return JsonResponse({success: 'Módulo cadastrado'})


def add_medico(request, nome, perfil, salario, expertise, atendimento, pontualidade):
	try:
		expertise = transform_num_to_char(expertise)
		atendimento = transform_num_to_char(atendimento)
		pontualidade = transform_num_to_char(pontualidade)
		m = Medico(nome=nome, perfil=perfil, salario=salario, expertise=expertise, atendimento=atendimento,
				   pontualidade=pontualidade)
		m.save()
		return JsonResponse({success: 'Médico cadastrado'})
	except Exception as e:
		return JsonResponse({err: str(e)})


def comprar_modulo(request, hospital, modulo):
	try:
		hospital = Hospital.objects.get(hash=hospital)
		response = hospital.comprar_modulo(modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)


def atribuir_medico_ao_modulo(request, hospital, medico, modulo):
	try:
		hospital = Hospital.objects.get(hash=hospital)
		response = hospital.atribuir_medico_ao_modulo(medico, modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)


def reembolsar_modulo(request, hospital, modulo):
	try:
		hospital = Hospital.objects.get(hash=hospital)
		response = hospital.vender_modulo(modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)


def get_all_configs(request):
	cfgs = {}
	i = 0
	for c in Configuracoes.objects.all():
		cfgs[i] = (c.to_simple_dict())
		i += 1
	return JsonResponse(cfgs)


def get_configs(request, config):
	try:
		configuracao = Configuracoes.objects.get(id=config)
		response = configuracao.to_dict()
	except ObjectDoesNotExist:
		response = {err: null_config}
	return JsonResponse(response)


def create_hospital(request, config, nome):
	h = Hospital.criar_hospital(config, nome)
	return JsonResponse(h)


def create_lobby(request, nome):
	h = Lobby.criar_lobby(nome)
	return JsonResponse(h)


def join_lobby(request, lobby, hospital):
	try:
		lobby = Lobby.objects.get(hash=lobby)
		response = lobby.join(hospital)
	except ObjectDoesNotExist:
		return JsonResponse({err: null_lobby})
	return JsonResponse(response)
